# Text to markdown
A converter that translate regular text to markdown

# Example text

```
Child section
1 blank line followed by a heading.


Section reset to parent
2 blank lines followed by a heading.


Section reset to start
A section to start is 3 blank lines followed by anything.



A heading
A heading is a single line that has i section reset above itself. It is also ledd than 80 characters wide, allowing a good fit into most terminals.


A paragraph
Everything else.
```
